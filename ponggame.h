#ifndef PONGGAME_H
#define PONGGAME_H

#include "pongball.h"
#include "pongbar.h"

class PongGame
{
public:
    PongGame();
    ~PongGame();
    void run();

private:
    PongBall *ball;
    PongBar *lbar,*rbar;
    sf::RenderWindow _win;
    bool upPressed,
        downPressed,
        ZPressed,
        SPressed;
};

#endif // PONGGAME_H
