#ifndef PONGBAR_H
#define PONGBAR_H

#define YSENSIBILITY 20

#include <SFML/Graphics.hpp>

class PongBar
{
public:
    PongBar(int xpos,int ypos, sf::Vector2u gameArea);
    void moveUp();
    void moveDown();
    void draw(sf::RenderTarget& target);
    sf::Shape& getShape() { return this->_shape; }

private:
    int _xpos, _ypos;
    sf::RectangleShape _shape;
    sf::Vector2u _gameArea;
};

#endif // PONGBAR_H
