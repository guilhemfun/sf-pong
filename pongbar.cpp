#include "pongbar.h"

PongBar::PongBar(int xpos, int ypos, sf::Vector2u gameArea) :
    _xpos(xpos), _ypos(ypos),
    _gameArea()
{
    // TODO : check whether we are out of bounds and retract from xpos and ypos accordingly
    _shape.setSize(sf::Vector2f(10,90));
    _shape.setFillColor(sf::Color::White);

    _gameArea.x = gameArea.x;
    _gameArea.y = gameArea.y;
}

void PongBar::moveUp()
{
    if (this->_ypos - YSENSIBILITY > 0)
    {
        this->_ypos -= YSENSIBILITY;
    }
    else
    {
        this->_ypos = 0;
    }
}

void PongBar::moveDown()
{
    if (this->_ypos + this->_shape.getSize().y + YSENSIBILITY < this->_gameArea.y)
    {
        this->_ypos += YSENSIBILITY;
    }
    else
    {
        this->_ypos = this->_gameArea.y - this->_shape.getSize().y;
    }
}

void PongBar::draw(sf::RenderTarget &target)
{
    this->_shape.setPosition(this->_xpos,this->_ypos);
    target.draw(this->_shape);
}
