#ifndef PONGBALL_H
#define PONGBALL_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "pongbar.h"

class PongBall
{
public:
    PongBall(int xpos,int ypos,int xspeed,int yspeed,
             sf::Vector2u gameArea);
    void step(float timestep, PongBar* lbar, PongBar* rbar);
    void draw(sf::RenderTarget& target);
    sf::Vector2u getScore();
    void reset();

private:
    float _xpos,_ypos,
        _xspeed,_yspeed;
    sf::RectangleShape _shape;
    sf::Vector2u _gameArea,
        _score;

    sf::SoundBuffer _bbCollisionBuffer,
        _scBuffer;
    sf::Sound _bbCollisionSound,
        _scoreChangedSound;
};

#endif // PONGBALL_H
