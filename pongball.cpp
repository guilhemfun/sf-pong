#include "pongball.h"
#include <stdlib.h>
#include <cmath>

PongBall::PongBall(int xpos, int ypos, int xspeed, int yspeed, sf::Vector2u gameArea) :
    _xpos(xpos),_ypos(ypos),
    _xspeed(xspeed), _yspeed(yspeed),
    _gameArea(0,0),
    _score(0,0)
{
    _shape.setSize(sf::Vector2f(10,10));
    _shape.setFillColor(sf::Color::White);

    _gameArea.x = gameArea.x;
    _gameArea.y = gameArea.y;


    // Todo check proper initialization of sounds
    // Todo maybe change for sf::Music
    this->_bbCollisionBuffer.loadFromFile("plop.ogg");
    this->_scBuffer.loadFromFile("beep.ogg");

    _bbCollisionSound.setBuffer(_bbCollisionBuffer);
    _scoreChangedSound.setBuffer(_scBuffer);

}

void PongBall::step(float timestep, PongBar *lbar, PongBar *rbar)
{
    int xstep = timestep * 100 * this->_xspeed;
    int ystep = timestep * 100 * this->_yspeed;
    bool collision = false;
    bool scoreChanged = false;

    // Deal with cases where the ball goes overboard (top & bottom)
    if (this->_ypos + ystep > this->_gameArea.y - this->_shape.getSize().y
            || this->_ypos + ystep < 0) {
        this->_yspeed *= -1;
        collision = true;
    } else {
        this->_ypos += ystep;
    }

    // Deal with cases where the ball goes overboard (left & right)
    if (this->_xpos + xstep > this->_gameArea.x - this->_shape.getSize().x)
    {
        this->_score.x += 1;
        scoreChanged = true;
        reset();
    } else if (this->_xpos + xstep < 0)
    {
        this->_score.y += 1;
        scoreChanged = true;
        reset();
    }
    else
    {
        this->_xpos += xstep;
    }

    // Handle collisions with bars
    if (this->_shape.getGlobalBounds().intersects(lbar->getShape().getGlobalBounds())
            || this->_shape.getGlobalBounds().intersects(rbar->getShape().getGlobalBounds()))
    {
        // Intersection with one of the bars
        this->_xspeed *= -1;
        this->_xpos -= 2*xstep;
        collision = true;
    }

    if(collision)
        this->_bbCollisionSound.play();
    if(scoreChanged)
        this->_scoreChangedSound.play();
}

void PongBall::draw(sf::RenderTarget &target)
{
    this->_shape.setPosition(this->_xpos,this->_ypos);
    target.draw(this->_shape);
}

sf::Vector2u PongBall::getScore()
{
    return this->_score;
}

void PongBall::reset()
{
    // Reset ball position
    this->_xpos = (_gameArea.x - _shape.getSize().x)/2;
    this->_ypos = (_gameArea.y - _shape.getSize().y)/2;

    // Randomize speeds
    srand(time(NULL));
    float a = 0.7 + static_cast<float>(rand()) /
            static_cast<float>(RAND_MAX/0.6);

    float b = sqrt(1-(1-a*a)*this->_xspeed/this->_yspeed);

    this->_xspeed *= a;
    this->_yspeed *= b;
}
