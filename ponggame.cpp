#include "ponggame.h"
#include <sstream>

#define CONFIG_XSPEED 2
#define CONFIG_YSPEED 2
#define CONFIG_XOFFSET 2

// How is this not standard
template<typename T>
std::string toString(const T& value)
{
    std::ostringstream oss;
    oss << value;
    return oss.str();
}

PongGame::PongGame() :
    _win(sf::VideoMode(800,600),"MAILLE PONGUE"),
    upPressed(false),downPressed(false),
    ZPressed(false),SPressed(false)
{
    this->ball = new PongBall(_win.getSize().x/2,_win.getSize().y/2,
                              CONFIG_XSPEED,CONFIG_YSPEED,
                              sf::Vector2u(800,600));
    this->lbar = new PongBar(CONFIG_XOFFSET,_win.getSize().y/2,
                             sf::Vector2u(800,600));
    this->rbar = new PongBar(_win.getSize().x - 10 -CONFIG_XOFFSET,_win.getSize().y/2,
                             sf::Vector2u(800,600));
}

PongGame::~PongGame()
{
    delete(this->ball);
    delete(this->rbar);
    delete(this->lbar);
}

void PongGame::run()
{
    sf::Clock clock;
    while (_win.isOpen())
    {
        // Deal with events
        sf::Event event;
        sf::Time time = clock.getElapsedTime();
        clock.restart();

        while (_win.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed :
                    _win.close();
                    break;
                case sf::Event::KeyPressed :
                    if  (event.key.code == sf::Keyboard::Up)
                        upPressed = true;
                    if (event.key.code == sf::Keyboard::Down)
                        downPressed = true;
                    if (event.key.code == sf::Keyboard::Z)
                        ZPressed = true;
                    if (event.key.code == sf::Keyboard::S)
                        SPressed = true;
                    break;
                case sf::Event::KeyReleased :
                    if  (event.key.code == sf::Keyboard::Up)
                        upPressed = false;
                    if (event.key.code == sf::Keyboard::Down)
                        downPressed = false;
                    if (event.key.code == sf::Keyboard::Z)
                        ZPressed = false;
                    if (event.key.code == sf::Keyboard::S)
                        SPressed = false;
                    break;
                default :
                    break;
            }
        }

        if (upPressed)
            this->rbar->moveUp();
        if (downPressed)
            this->rbar->moveDown();
        if (ZPressed)
            this->lbar->moveUp();
        if (SPressed)
            this->lbar->moveDown();


        // Clear the window
        _win.clear(sf::Color::Black);

        this->ball->step(time.asSeconds(),lbar,rbar);

        // Deal with the case where the ball goes through a

        this->ball->draw(_win);
        this->lbar->draw(_win);
        this->rbar->draw(_win);

        // Print score
        sf::Vector2u score = this->ball->getScore();
        sf::Font thefont;
        thefont.loadFromFile("bauerb.ttf");

        // Init and config score text
        sf::Text scoreText;
        scoreText.setString(toString(score.x) + " | " + toString(score.y));
        scoreText.setColor(sf::Color::White);
        scoreText.setFont(thefont);
        scoreText.setPosition(sf::Vector2f(_win.getSize().x/2-scoreText.getLocalBounds().width/2,_win.getSize().y-scoreText.getCharacterSize() - 2));

        _win.draw(scoreText);

        // End the current frame
        _win.display();
    }
}
