TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /home/arvil/customLib/SFML/2.2/include

SOURCES += main.cpp \
    ponggame.cpp \
    pongball.cpp \
    pongbar.cpp

HEADERS += \
    ponggame.h \
    pongball.h \
    pongbar.h

LIBS += -L/home/arvil/customLib/SFML/2.2/lib \
     -lsfml-system \
     -lsfml-graphics \
     -lsfml-window \
     -lsfml-audio
